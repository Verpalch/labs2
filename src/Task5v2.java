import java.util.Scanner;

public class Task5v2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Your value with 5 digits: ");
        int number = scanner.nextInt();
        System.out.print("Digit number: ");
        int digit = scanner.nextInt();

// С циклом		
        for (int i = 1; i < digit; i++) number/=10;
        number%=10;
        System.out.println(number);
    }
}