import java.util.Scanner;

public class Task3 {public static void main(String[] args) {
    int a,b,c,d;
    Scanner scanner = new Scanner(System.in);
    System.out.print("Your choice: ");
    a = scanner.nextInt();
    b = scanner.nextInt();
    c = scanner.nextInt();
    d = scanner.nextInt();

    //Без условия
    boolean result = (a%2==0 && b%4==0) || (c%3==0  && d%3 !=0);
    System.out.println(result);

//С условием
    if (a%2==0 && b%4==0)
        System.out.print("true");
    else if (c%3==0  && d%3 !=0)
        System.out.print("true");
    else
        System.out.print("false");
}
}
