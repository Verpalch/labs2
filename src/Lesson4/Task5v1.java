import java.util.Scanner;

public class Task5v1 {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in);
        //ВВести число
        System.out.print("Your value: ");
        int number = scanner.nextInt();
        System.out.print("Digit number: ");
        int digit = scanner.nextInt();

// С тернарным оператором
        System.out.println(digit==1 ? number%10 : (digit == 2? (number/10)%10 : (digit == 3? (number/1000)%10 : (digit == 4? (number/100)%10 : (digit == 5? number/10000 : "fail")))));
    }
}
